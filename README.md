# Basic Front-End Boilerplate for Static Sites
## Currently features
* NPM for Gulp and Bower installations (and various gulp plugins)
* Bower for bootstrap-sass and normalize package management
* Gulp for:
    * Converting SCSS to CSS and watching for changes
    * Adding CSS prefixes for browser support
    * Minifying HTML
    * Minifying SCSS
* Src folder contains raw unminified assets and raw HTML/SCSS/CSS
* Dist folder contains minified HTML, CSS

## Instructions
* Install NPM
	* `sudo apt-get install node`, or `brew install node` (for OS X)
* Install Bower and Gulp
	* `npm install bower` (prefix with `sudo` if that fails)
	* `npm install gulp` (prefix with `sudo` if that fails)
* Install required project dependencies
	* `npm install`
	* `bower install`
* Open `index.html` file in the `dist/` folder.
* Modify files in the `src/` folder, and run `gulp` after. <br>
NOTE: `gulp default:watch` currently tracks changes in `sass/*.scss` files and changes to `.html` files in the `src/` directory to run a minifier.

## TODO:
* Make gulpfile.js less hacky, easier to do when Gulp 4 comes out.
* Add templating engine for HTML, preferably handlebars.
* Improve HTML structuring
* More basic CSS
* Use gulp to minify images and JS for the Dist folder