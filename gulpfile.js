var gulp = require('gulp'); // DUH
var sass = require('gulp-sass'); // SASS --> CSS
var uglify = require('gulp-uglify'); // JS MINIFIER
var uglifycss = require('gulp-uglifycss'); // CSS MINIFIER
var runSequence = require('run-sequence'); // TEMPORARY, I PROMISE
var htmlmin = require('gulp-htmlmin'); // HTML MINIFIER
var postcss = require('gulp-postcss')
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');

// runSequence and sync are temporary solutions until Gulp 4.0.0 release.
gulp.task('default', function(callback) {
  runSequence('sass',
              'sync',
              'cssprefixes',
              'sync',
              'css',
              callback)
  });

gulp.task('default:watch', function () {
  gulp.watch('./src/sass/**/*.scss', ['default']);
  gulp.watch('./src/*.html', ['minify-html']);
});

// SASS TO CSS
gulp.task('sass', function(){
    console.log("Hello!");
    gulp.src('./src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/css'))
});

// ADD CSS VENDOR PREFIXES
gulp.task('cssprefixes', function () {
	return gulp.src('./src/css/*.css')
    .pipe(sourcemaps.init())
    .pipe(postcss([ autoprefixer({ browsers: ['last 2 versions'] }) ]))
    .pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('src/css'));
});


// MINIFYCSS
gulp.task('css', function () {
  gulp.src('./src/css/*.css')
    .pipe(uglifycss({
      "max-line-len": 80
    }))
    .pipe(gulp.dest('./dist/css'));
});




// HTML
gulp.task('minify-html', function() {
  return gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'))
});


// SYNC
gulp.task('sync', function (cb) {  
    // setTimeout could be any async task
    setTimeout(function () {
        cb();
    }, 1000);
});
